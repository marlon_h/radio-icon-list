let radioIcons = [];

class Controller {
    constructor() {
        this.active = false;
        this.$onInit = this._onInit;
    }

    onChange() {
        this.radioIconListController.onRadioIconChange(this);
    }

    _onInit() {
        this.radioIconListController.register(this);
    }    
}

export default Controller;