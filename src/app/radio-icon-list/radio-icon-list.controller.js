let radioIcons = [];

class Controller {
    constructor() {
        // Resolve active radio icons after they are finally registered    
        this.$postLink = () => this._setActiveRadioIcons();
    }

    register(radioIcon) {
        radioIcons.push(radioIcon);
    }

    onRadioIconChange(radioIcon) {    
        this._setActiveRadioIcons();
    }

    _setActiveRadioIcons() {    
        let selectedIndex = radioIcons.findIndex(radioIcon => radioIcon.value === this.ngModel);

        radioIcons.forEach((radioIcon, index) => {
            radioIcon.active = selectedIndex < index;
        });
    }
}

export default Controller;