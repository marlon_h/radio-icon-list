import RadioIconList from './radio-icon-list.component';
import RadioIcon from './radio-icon.component';

export default angular
    .module('radioIconLis', [])
    .component('radioIconList', RadioIconList)
    .component('radioIcon', RadioIcon)
    .name;