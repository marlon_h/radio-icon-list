import template from './radio-icon.component.html';
import controller from './radio-icon.controller';
import './radio-icon.less';

let component = {
    template,
    controller,
    transclude: true,
    require: {
        radioIconListController: '^radioIconList'
    },
    bindings: {
        name: '@',
        value: '@'
    }
};

export default component;