import template from './radio-icon-list.component.html';
import controller from './radio-icon-list.controller';

let component = {
    template,
    controller,
    transclude: true,
    bindings: {
        ngModel: '='
    }
};

export default component;