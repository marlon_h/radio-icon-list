import template from './app.component.html';
import './app.less';

let appComponent = {
    template,
    controller: function () {
        this.foo = '4';
    }
};

export default appComponent;