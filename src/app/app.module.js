import RadioIconList from './radio-icon-list/radio-icon-list.module';

import AppComponent from './app.component';

angular
    .module('app', [
        RadioIconList
    ])
    .component('appRoot', AppComponent);